package fragments


import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.database.Observable
import android.location.Criteria
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import api.ActiveUser
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import interfaces.OnlineDrivers


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [MapFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class MapFragment : Fragment(), OnMapReadyCallback, OnlineDrivers, ActivityCompat.OnRequestPermissionsResultCallback {
    var drivers: Observable<String>? = null
    var locationManager: LocationManager? = null

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when(requestCode){
            Helpers.REQUEST_CODE -> {
                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(context, "Location permissions it's a must", Toast.LENGTH_SHORT).show()
                }
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onPositionDriverUpdate(driversString: String) {
        print(drivers)
    }

    private lateinit var mMap: GoogleMap

    private fun askForMyLocation(): Location? {
        if (locationManager == null){
            context?.let {
                locationManager = it.getSystemService(Context.LOCATION_SERVICE) as LocationManager
            }
        }

        context?.let { context ->
            if (Helpers.checkPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)){
                locationManager?.let {
                    return it.getLastKnownLocation(
                        it.getBestProvider(Criteria(), false))
                }
            }
        }

        return null
    }

    override fun onMapReady(p0: GoogleMap?) {
        mMap = p0!!

        askForMyLocation()?.let {
            val newCurrentLocation: LatLng = LatLng(it.latitude, it.longitude)

            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(CameraPosition.builder()
                .target(newCurrentLocation)
                .zoom(15F)
                .build()))
        }

        context?.let {
            if (Helpers.checkPermission(it, Manifest.permission.ACCESS_FINE_LOCATION)){
                mMap.isMyLocationEnabled = true
            }
        }
    }

    // TODO: Rename and change types of parameters
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view =  inflater.inflate(com.duotipo.bruline.R.layout.fragment_map, container, false)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        context?.let { Helpers.makePermissionRequest(it, Manifest.permission.ACCESS_FINE_LOCATION) }
        val mapFragment = childFragmentManager
            .findFragmentById(com.duotipo.bruline.R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
//        val mapFragment = childFragmentManager.findFragmentById(R.id.map2) as SupportMapFragment?
//        ActiveUser.delegate = this
//        context?.let { ActiveUser.downloadOnlineDrivers(it) }
        return view
    }



    companion object {
        init {

        }
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance() =
            MapFragment()
    }
}
