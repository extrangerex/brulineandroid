package fragments


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import com.duotipo.bruline.R
import java.time.LocalDateTime
import java.util.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

class PointOfInterests : Fragment() {

    var txtHello: TextView? = null

    fun getHello() {
        val hourOfTheDay: Int = Calendar.HOUR_OF_DAY
        txtHello?.let {
            when(hourOfTheDay){
                in 0..6, in 18..24  -> {
                    it.text = getString(R.string.good_night)
                }
                in 7..11 -> {
                    it.text = getString(R.string.good_day)
                }
                in 12..17 -> {
                    it.text = getString(R.string.good_afternoon)
                }
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view =  inflater.inflate(R.layout.fragment_point_of_interests, container, false)

        return view;
    }


}
