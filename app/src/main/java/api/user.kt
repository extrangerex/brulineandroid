package api

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.reflect.TypeToken
import interfaces.OnlineDrivers
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import models.*
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import services.Api
import java.util.*
import kotlin.coroutines.suspendCoroutine
import kotlin.system.measureTimeMillis

class User(callback: api.Callback) : Callback<Response>{

    private var globalResponse: Response? = null
    private var apiService : Api? = null
    private var callback: api.Callback? = callback

    override fun onFailure(call: Call<Response>, t: Throwable) {
        globalResponse = Response(success = ResponseStates.ERR, data = t.message.toString())
    }

    override fun onResponse(call: Call<Response>, response: retrofit2.Response<Response>) {
        if (!response.isSuccessful) {
            globalResponse =  Response(success = ResponseStates.ERR, data = "Bad Response")
        }
        print("ARRAY " + response.body())
        globalResponse = when(response.code()){
            200 -> {
                Response(success = ResponseStates.SUCCESS, data = response.body()!!.data)
            }
            404 -> {
                Response(success = ResponseStates.ERR, data = if(response.body() != null) response.body()!!.data else Parser.convertJsonToResponse(json = response.errorBody()!!.string()).data )
            }
            else -> {
                Response(success = ResponseStates.ERR, data = response.errorBody()!!.string())
            }
        }

        callback!!.onGlobalResponse(globalResponse!!, response.code())
        Log.v("ServerResponse: " , globalResponse!!.data.toString())
    }

    init {
        apiService = Session.getApiReference()
    }

    suspend fun performDriverSignUp(
        name: String,
        userName: String,
        photoUrl: String,
        emailAddress: String,
        password: String,
        birthDate: String
    )   {
        val driverToSignUp = DriverSignUpModal(
            name = name,
            photoUrl = photoUrl,
            userName = userName,
            emailAddress = emailAddress,
            password = password,
            birthDate = birthDate)

        apiService!!.signUpDriver(driverModal = driverToSignUp)
            .enqueue(this)
    }

    fun performLogIn(
        emailAddress : String,
        password: String
    ) {
        measureTimeMillis {
            val login = LoginModal(emailAddress, password)
            apiService!!.signIn(login).enqueue(this)
        }
    }

    suspend fun downloadOnlineDrivers(context: Context) {
        val token = Helpers.returnToken(context)
        if(token != false.toString()){
            apiService!!.getOnlineDrivers(token!!).enqueue(this)
        }
    }

    suspend fun getOnlineDrivers(token: String) {

    }
}

object ActiveUser{

    var delegate: OnlineDrivers? = null

    fun downloadOnlineDrivers(context: Context) {
        runBlocking {
            coroutineScope {
                User(object: api.Callback {
                    override fun onGlobalResponse(response: Response, code: Int) {
                        delegate!!.onPositionDriverUpdate(response.data.toString())
                    }
                }).downloadOnlineDrivers(context)
//                delay(4000)
//                downloadOnlineDrivers(context)
            }
        }
    }
}

interface Callback{
    fun onGlobalResponse(response: Response, code: Int)
}