package api

import retrofit2.create
import services.Api
import services.Retrofit

object Session{
    var apiService: Api? = null

    fun getApiReference() : Api{
        if (apiService == null){
            apiService = Retrofit.getRetrofitInstance()!!.create<Api>(Api::class.java)
            return apiService!!
        }
        return apiService!!
    }
}