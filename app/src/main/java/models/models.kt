package models

import com.google.gson.annotations.SerializedName
import org.json.JSONObject
import java.util.*

data class User (
    @SerializedName("name")
    val name: String,
    @SerializedName("type")
    val type: String,
    @SerializedName("userName")
    val userName: String,
    @SerializedName("emailAddress")
    val emailAddress: String,
    @SerializedName("birthDate")
    val birthDate: Date,
    @SerializedName("createdAt")
    val createdAt: Date
)

//ENDPOINT: /online
//FOR ONLINE BEING POSTED
//YOU MUST SEND TO THE ENDPOINT THE PARAMS
//lat, lng
//ENDPOINT RETURNS AN OBJECT OF OnlineModal
//ID MUST BE SAVED
//Calling Delete Method with this endpoint
//passing the online id going to destroy your
//current id session

data class OnlineModal(
    @SerializedName("vehycle")
    val vehycle: VehycleModal,
    @SerializedName("isOnRide")
    val isOnRide: Boolean,
    @SerializedName("currentPos")
    val currentPos: List<Double>
)

data class VehycleTypeModal (
    @SerializedName("typeName")
    val typeName: String,
    @SerializedName("icon")
    val icon: String,
    @SerializedName("tax")
    val tax: Double
)

//ENDPOINT : /driver/vehycle
//PARAMS: emailAddress, VehycleTypeModal
//HEADER: Token REQUIRED!!
//IF IS SAVED API GOING TO RETURN vehycleType Object

data class VehycleModal(
    @SerializedName("vehycleType")
    val vehycleType: String,
    @SerializedName("manufacturer")
    val manufacturer: String,
    @SerializedName("model")
    val model: String,
    @SerializedName("productionYear")
    val productionYear: Int,
    @SerializedName("state")
    val state: Boolean,
    @SerializedName("owner")
    val owner: String,
    @SerializedName("registrationNo")
    val registrationNo: String
)

data class LoginModal(
    @SerializedName("emailAddress")
    val emailAddress: String,
    @SerializedName("password")
    val password: String
)

data class DriverSignUpModal(
    @SerializedName("name")
    val name : String,
    @SerializedName("type")
    val type : String = "driver",
    @SerializedName("userName")
    val userName : String,
    @SerializedName("photoUrl")
    val photoUrl : String,
    @SerializedName("emailAddress")
    val emailAddress : String,
    @SerializedName("password")
    val password : String,
    @SerializedName("birthDate")
    val birthDate : String
)

data class Response(
    @SerializedName("success")
    val success: Any,
    @SerializedName("data")
    val data: Any
)

object Parser{
    fun convertJsonToResponse(json: String) : Response{
        if (json.contains("</html>")){
            return Response(
                success = ResponseStates.ERR,
                data = "Bad Response. Try again Later"
            )
        }
        val jsonObject: JSONObject = JSONObject(json)
        return Response(
            success = jsonObject.getString("success"),
            data = jsonObject.getString("data")
        )
    }
}