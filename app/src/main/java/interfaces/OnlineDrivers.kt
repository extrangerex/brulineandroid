package interfaces

import models.OnlineModal

interface OnlineDrivers {
    fun onPositionDriverUpdate(driversString: String)
}