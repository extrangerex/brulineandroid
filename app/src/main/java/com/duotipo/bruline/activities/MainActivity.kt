package com.duotipo.bruline.activities

import android.content.Context
import android.content.Intent
import android.opengl.Visibility
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import api.ActiveUser
import api.Callback
import api.Session
import com.duotipo.bruline.R
import kotlinx.android.synthetic.main.activity_main.*
import models.Response
import models.ResponseStates
import kotlin.coroutines.*

class MainActivity : AppCompatActivity() {

    private var _btnLogin : Button? = null
    private var _txtEmailAddress : EditText? = null
    private var _txtPassword: EditText? = null
    private var _progressBar: ProgressBar? = null
    private var _inputLinearLayout: LinearLayout? = null

    fun isLoading(loading: Boolean = false){
        if(loading){
            inputsLinearLayout!!.visibility = View.INVISIBLE
            _progressBar!!.visibility = View.VISIBLE
        }else{
            inputsLinearLayout!!.visibility = View.VISIBLE
            _progressBar!!.visibility = View.INVISIBLE
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        closeKeyboard()

        _btnLogin = findViewById(R.id.btnLogin)
        _txtEmailAddress = findViewById(R.id.txtEmailAddress)
        _txtPassword = findViewById(R.id.txtPassword)
        _progressBar = findViewById(R.id.progressBar)
        _inputLinearLayout = findViewById(R.id.inputsLinearLayout)

        _btnLogin!!.setOnClickListener(object: View.OnClickListener {
            override fun onClick(v: View?) {
                logIn()
            }

        })
    }


    fun closeKeyboard(){
        val view: View? = this.currentFocus
        if(view != null) {
            val imm : InputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }


    fun logIn(){

        if(Helpers.checkEmptyEditText(_txtEmailAddress!!) &&
        Helpers.checkEmptyEditText(_txtPassword!!)){
            isLoading(true)
            closeKeyboard()
            api.User(
                object : Callback{
                    override fun onGlobalResponse(response: Response, code: Int) {
                        when(response.success){
                            ResponseStates.ERR -> {
//                                SOMETHING WRONG
                                if(code == 404){
                                    txtEmailAddress.error = response.data.toString()
                                }
//                                Toast.makeText(baseContext, response.data as String, Toast.LENGTH_SHORT).show()
                            }
                            ResponseStates.SUCCESS -> {
//                                LOGIN EXITOSO
                                if(Helpers.saveToken(context = baseContext, token = response.data.toString())){
//                                    print("saved")
                                    val intent: Intent = Intent(baseContext, DashboardActivity::class.java)
                                    startActivity(intent)
                                }else{
                                    txtEmailAddress.error = "Something wrong. Try again later"
                                }
                            }
                        }
                        isLoading(false)
                    }
                }
            ).performLogIn(
                txtEmailAddress.text.toString(),
                txtPassword.text.toString()
            )
        }
    }
}
