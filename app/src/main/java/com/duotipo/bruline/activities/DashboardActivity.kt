package com.duotipo.bruline.activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
//import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v7.app.ActionBar
import android.view.MenuItem
import com.duotipo.bruline.R
import fragments.HistoryFragment
import fragments.MapFragment
import fragments.SettingFragment


class DashboardActivity : AppCompatActivity() {

    var navigationView: BottomNavigationView? = null
//    lateinit var toolbar: ActionBar
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)
//        toolbar = supportActionBar!!
        navigationView = findViewById(R.id.bottom_navigation)

        navigationView!!.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

        openFragment(MapFragment())
    }


    private fun openFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.menu_settings -> {
//                toolbar.title = "Settings"
                val songsFragment = SettingFragment.newInstance()
                openFragment(songsFragment)
                return@OnNavigationItemSelectedListener true
            }
            R.id.menu_map -> {
//                toolbar.title = "Settings"
                val songsFragment = MapFragment.newInstance()
                openFragment(songsFragment)
                return@OnNavigationItemSelectedListener true
            }
            R.id.menu_history -> {
//                toolbar.title = "History"
                val songsFragment = HistoryFragment.newInstance()
                openFragment(songsFragment)
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }
}
