import android.app.Activity
import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.view.View
import android.widget.EditText
import android.widget.Toast
import api.ActiveUser
import kotlinx.coroutines.CoroutineScope

object Helpers{
    var prefs: SharedPreferences? = null
    var bruline_prefs_string = "bruline_prefs"
    val REQUEST_CODE = 101

    fun checkPermission(context: Context, PERMISSION_ID: String): Boolean {
        val permission = ContextCompat.checkSelfPermission(
            context, PERMISSION_ID)

        return permission == PackageManager.PERMISSION_GRANTED
    }

    fun makePermissionRequest(context: Context, PERMISSION_ID: String) {
        val permission = ContextCompat.checkSelfPermission(
            context, PERMISSION_ID)

        if (permission != PackageManager.PERMISSION_GRANTED) {
            print("PERMISSION NOT GRANTED")
            ActivityCompat.requestPermissions(
                context as Activity,
                arrayOf(PERMISSION_ID),
                REQUEST_CODE
            )
        }
    }

    fun createPrefInstanceifNotExists(context: Context){
        if (prefs == null)
            prefs = context.getSharedPreferences(bruline_prefs_string, MODE_PRIVATE)
    }

    fun returnToken(context: Context): String? {
        createPrefInstanceifNotExists(context)
        return if(prefs!!.getString("token", null) == null) false.toString() else prefs!!.getString("token", null)
    }

    fun saveToken(context: Context, token : String): Boolean {
        createPrefInstanceifNotExists(context)
        val editor = prefs!!.edit()
        editor.putString("token", token)
        if(editor.commit())
            return true
        return false
    }

    fun checkEmptyEditText(editText: EditText, errorString: String = "This field can't be empty") : Boolean {
        var isValid : Boolean = false
        if(editText.text.isEmpty()) {
            editText.error = errorString
        } else {
            isValid = true
        }
        return isValid
    }


}