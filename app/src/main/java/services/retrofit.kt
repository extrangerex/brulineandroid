package services

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object Retrofit{
    var retrofit: Retrofit? = null;

    fun configure() : Retrofit {
        return Retrofit.Builder()
            .baseUrl(Routes.base)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .build();
    }

    fun getRetrofitInstance(): Retrofit? {
        if (retrofit == null) {
            retrofit = configure()
        }
        return  retrofit
    }
}