package services

class Routes{
    companion object {
        const val base: String = "http://duotipo-bruline.herokuapp.com"
        const val login: String = "/user/login"
        const val driver: String = "/user/driver"
        const val online: String = "/online"
    }
}