package services

import com.google.gson.annotations.SerializedName
import models.DriverSignUpModal
import models.LoginModal
import models.Response
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST

interface Api{
    @POST(Routes.login)
    fun signIn(@Body loginModal: LoginModal) : Call<Response>
    @POST(Routes.driver)
    fun signUpDriver(@Body driverModal: DriverSignUpModal) : Call<Response>
    @GET(Routes.online)
    fun getOnlineDrivers(@Header("token") token: String) : Call<Response>
}